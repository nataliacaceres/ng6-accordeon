import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './components/layout/layout.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { AccordionComponent } from './components/accordion/accordion.component';
import { AccordionConfigurationComponent } from './components/accordion-configuration/accordion-configuration.component';
import { SortableItemComponent } from './components/sortable-item/sortable-item.component';
import { DragSortableDirective } from './directives/drag-sortable/drag-sortable.directive';
import { EditableLabelComponent } from './components/editable-label/editable-label.component';
import {FormsModule} from '@angular/forms';

@NgModule({  
  declarations: [LayoutComponent, HeaderComponent, FooterComponent, AccordionComponent, AccordionConfigurationComponent, SortableItemComponent, DragSortableDirective, EditableLabelComponent],
  imports: [
    CommonModule, FormsModule
  ],
  exports: [LayoutComponent, SortableItemComponent, AccordionConfigurationComponent]
})
export class UiModule { }
