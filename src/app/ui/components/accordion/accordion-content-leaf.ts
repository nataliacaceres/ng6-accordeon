import { AccordionContent } from "./accordion-content";

export class AccordionContentLeaf implements AccordionContent{
    Title: String;
    Content: String;    
    ImageUrl: String;

    /**
     *
     */
    constructor(obj?: Partial<AccordionContentLeaf>) {
        Object.assign(this, obj);
    }
}
