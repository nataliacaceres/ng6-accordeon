import { AccordionContent } from "./accordion-content";

export class AccordionContentBranch implements AccordionContent{
    Title: String;
    ContentBranch: AccordionContent;
}
