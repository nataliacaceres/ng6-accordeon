import { Component, OnInit, Input } from '@angular/core';
import { AccordionContent } from '../accordion/accordion-content';

@Component({
  selector: 'app-sortable-item',
  templateUrl: './sortable-item.component.html',
  styleUrls: ['./sortable-item.component.css']
})
export class SortableItemComponent implements OnInit {

  @Input() item: AccordionContent;
  constructor() { }

  ngOnInit() {
  }

  titleChanged(event: string){
    this.item.Title = event;
  }

}
