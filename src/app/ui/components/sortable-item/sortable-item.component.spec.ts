import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SortableItemComponent } from './sortable-item.component';

describe('SortableComponentComponent', () => {
  let component: SortableItemComponent;
  let fixture: ComponentFixture<SortableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SortableItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SortableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
