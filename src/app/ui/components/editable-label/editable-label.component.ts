import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-editable-label',
  templateUrl: './editable-label.component.html',
  styleUrls: ['./editable-label.component.css']
})
export class EditableLabelComponent implements OnInit {

  @Input() content: String;
  @Output() contentChanged = new EventEmitter<String>();

  editing: Boolean = false;

  constructor() { }

  ngOnInit() {
  }

  onLabelClick(){
    this.editing = true;
  }

  onBlur(){
    this.editing = false;
    this.contentChanged.emit(this.content);
  }
}
