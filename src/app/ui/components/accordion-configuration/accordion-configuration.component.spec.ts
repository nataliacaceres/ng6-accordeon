import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccordionConfigurationComponent } from './accordion-configuration.component';

describe('AccordionConfigurationComponent', () => {
  let component: AccordionConfigurationComponent;
  let fixture: ComponentFixture<AccordionConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccordionConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccordionConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
