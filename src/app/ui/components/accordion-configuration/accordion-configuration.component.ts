import { Component, OnInit } from '@angular/core';
import { SortableItemComponent } from '../sortable-item/sortable-item.component';
import { AccordionContent } from '../accordion/accordion-content';
import { AccordionContentLeaf } from '../accordion/accordion-content-leaf';

@Component({
  selector: 'app-accordion-configuration',
  templateUrl: './accordion-configuration.component.html',
  styleUrls: ['./accordion-configuration.component.css']
})
export class AccordionConfigurationComponent implements OnInit {

  sortableItems : AccordionContent[];

  constructor() {
    this.sortableItems = [
      new AccordionContentLeaf({
        Title: 'Click me',
        ImageUrl: 'https://picsum.photos/500/?random'
      }),
      new AccordionContentLeaf({
        Title: 'and then just touch me',
        ImageUrl: 'https://picsum.photos/500/?random'
      }),
      new AccordionContentLeaf ({
        Title: 'so I can get my',
        ImageUrl: 'https://picsum.photos/500/?random'
      }),
      new AccordionContentLeaf ({
        Title: 'title changed',
        ImageUrl: 'https://picsum.photos/500/?random'
      }),
      new AccordionContentLeaf ({
        Title: 'no copyright violated',
        ImageUrl: 'https://picsum.photos/500/?random'
      })
    ];
  }


  ngOnInit() {
  }

  onDropSortable(event) {
    console.log(event);
  }

}
