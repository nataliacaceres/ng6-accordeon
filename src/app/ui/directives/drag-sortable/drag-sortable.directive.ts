import { Directive, OnInit, OnDestroy, Input, Output, EventEmitter, HostBinding, HostListener } from '@angular/core';
import { Subject } from 'rxjs';
import { DragService, DragSource } from '../../services/drag/drag.service';
import {take, takeWhile, takeUntil} from 'rxjs/operators';

export class DropSortableEvent {
  data: any;
  newIndex: number;
}

@Directive({
  selector: '[appDragSortable]'
})
export class DragSortableDirective implements OnInit, OnDestroy{

  dragImg: HTMLElement;
  dragImgSize: {width: number, height: number};

  private ngUnsubsCribe: Subject<void> = new Subject<void>();

  @Input() item;
  @Input() itemIndex;
  @Input() sortableElSelector: string;

  @Output() dropSortable: EventEmitter<DropSortableEvent> = new EventEmitter();
  
  @HostBinding('draggable') get draggable(){
    return true;
  }
  @HostBinding('class.collapsed') collapsed = false;
  @HostBinding('class.dragging') dragging = false;
  
  @HostListener('dragstart', ['$event']) onDragStart(event){ //event is a DragEvent
    this.dragImg = event.target.querySelector('.drag-image');
    if(this.dragImg){
      this.dragImgSize = {
        width: this.dragImg.offsetWidth,
        height: this.dragImg.offsetHeight
      };
    }

    this.dragging = true;
    this.dragService.dragSource$.next({element: event.target, index: this.itemIndex});

    this.dragService.collapse$.next(true);
    setCustomGhostImage.call(this);

    try {
      event.dataTransfer.setData('application/json', JSON.stringify(this.item));
    } catch (e) {
      event.dataTransfer.setData('text', JSON.stringify(this.item));
    }

    function setCustomGhostImage(){
      event.dataTransfer.setDragImage(this.dragImg, this.dragImgSize.width/2, this.dragImgSize.height/2);
    }
  }

  @HostListener('dragenter', ['$event']) onDragEnter(event){
    this.dragService.dragSource$.pipe(takeWhile(dragSource => Boolean(dragSource)), take(1))
      .subscribe((dragSource: DragSource) => {
        const dragEl = dragSource.element;
        const dragElIndex = dragSource.index;
        const textNodeType = 3;
        const target = event.target.nodeType === textNodeType ? event.target.parentNode : event.target;
        const sortableEl = target.closest(this.sortableElSelector);
        
        if(dragEl !== sortableEl){
          if(isBefore(dragEl, sortableEl)){
            sortableEl.parentNode.insertBefore(dragEl, sortableEl);
            this.dragService.dragSource$.next(Object.assign({}, dragSource, {index: dragElIndex -1}));
          }
          else{
            sortableEl.parentNode.insertBefore(dragEl, sortableEl.nextSibling);
            this.dragService.dragSource$.next(Object.assign({}, dragSource, {index: dragElIndex +1}));
          }
        }
      });
    
    event.preventDefault();

    /**
     * Indicates if second node is before the first one
     * @param firstNode 
     * @param secondNode 
     */  
    function isBefore(firstNode, secondNode): boolean{
      if(firstNode.parentNode === secondNode.parentNode){
        for (let cur = firstNode; cur; cur=cur.previousSibling){
          if(cur === secondNode){
            return true;
          }
        }
      }
      return false;
    }
  }

  @HostListener('dragover', ['$event']) onDragOver(event) {
    event.preventDefault();
  }

  @HostListener('dragend', ['$event']) onDragEnd(event){
    this.dragging = false;
    this.dragService.dragSource$.next(null);
    this.dragService.collapse$.next(false);    
  }

  @HostListener('drop', ['$event']) onDrop(event){
    this.dragService.dragSource$
      .pipe(takeWhile(dragSource => Boolean(dragSource)), take(1))
      .subscribe((dragSource: DragSource) => {
        let data;
        try{
          data = JSON.parse(event.dataTransfer.getData('application/json'));
        } catch(e){
          data = JSON.parse(event.dataTransfer.getData('text'))
        }
        this.dropSortable.emit({data, newIndex: dragSource.index});
      });
  }

  ngOnDestroy(): void {
    this.ngUnsubsCribe.next();
    this.ngUnsubsCribe.complete();
  }

  ngOnInit(): void {
    this.dragService.collapse$
      .pipe(takeUntil(this.ngUnsubsCribe))
      .subscribe((collapsedState: boolean) => {
        this.collapsed = collapsedState;
      })
  }

  constructor(private dragService: DragService) { }
}
